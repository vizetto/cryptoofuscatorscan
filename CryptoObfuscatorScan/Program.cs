﻿using Microsoft.Build.Construction;
using Microsoft.Build.Evaluation;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace CryptoObfuscatorScan
{
    internal class Program
    {
        private static readonly string microsoftNuGetPath = Environment.ExpandEnvironmentVariables( @"%userprofile%\.nuget\packages" );

        private static readonly string devexpressNuGetPath = Environment.ExpandEnvironmentVariables( @"%PROGRAMFILES(X86)%\DevExpress 21.1\Components\Offline Packages" );

        private static readonly string[] baseNuGetPaths = 
        {
            microsoftNuGetPath,
            devexpressNuGetPath
        };

        private static readonly string[] frameworks =
        {
            "net48",
            "net472",
            "net471",
            "net47",
            "net462",
            "net461",
            "net46",
            "net452",
            "net451",
            "net45",
            "net403",
            "net40",
            "net35",
            "net30",
            "net20",
            "net11",
            "netstandard2.0"
        };

        private static readonly string[] subdirs =
        {
            "lib",
            "cef",
            "tasks",
            "build"
        };

        private static string BuildNugetPath( string package, string version )
        {
            foreach ( var nuGetPath in baseNuGetPaths )
            {
                if ( Directory.Exists( nuGetPath ) )
                {
                    var packagePath = Path.Combine( nuGetPath, package, version );

                    if ( Directory.Exists( packagePath ) )
                    {
                        foreach ( var subdir in subdirs )
                        {
                            var libPath = Path.Combine( packagePath, subdir );

                            if ( Directory.Exists( libPath ) )
                            {
                                foreach ( var framework in frameworks )
                                {
                                    var fullpath = Path.Combine( libPath, framework );

                                    if ( Directory.Exists( fullpath ) )
                                        return fullpath;
                                };

                                return libPath;
                            }

                        }

                        return packagePath;
                    }

                }

            };

            return null;
        }

        private static string GetMetadata( ProjectItem item, string name )
        {
            foreach ( var property in item.Metadata )
            {
                if ( property.Name == name )
                    return property.EvaluatedValue;
            };

            return string.Empty;
        }

        static void Main( string[] args )
        {
            var nugetHashSet = new HashSet<string>( StringComparer.CurrentCultureIgnoreCase );

            var solutionFile = SolutionFile.Parse( args[0] );

            if ( solutionFile != null )
            {
                foreach ( var projectInfo in solutionFile.ProjectsInOrder )
                {
                    if ( File.Exists( projectInfo.AbsolutePath ) )
                    {
                        var project = new Project( projectInfo.AbsolutePath );

                        if ( project != null )
                        {
                            foreach ( var item in project.Items )
                            {
                                if ( item.ItemType != "PackageReference" )
                                    continue;

                                var version = GetMetadata( item, "Version" );

                                var searchPath = BuildNugetPath( item.EvaluatedInclude, version );

                                if ( searchPath != null )
                                    nugetHashSet.Add( searchPath );
                            }

                        }

                    }

                }

            }

            var nugetArray = nugetHashSet.ToArray( );

            Array.Sort( nugetArray );

            foreach ( var nugetItem in nugetArray )
                Console.Out.WriteLine( $"<SearchDirectory Path = \"{nugetItem}\" />" );

        }

    }

}
